VERSION = 3.00
SONAME_VERSION = 0.0
SONAME = libasciimap.so.3
SONAME_NOVERSION = libasciimap.so
DESTDIR ?= /
PREFIX ?= usr
LIBDIR ?= lib
INCLUDEDIR ?= include
BINDIR ?= bin
SHAREDIR ?= share
LIB_DEST ?= $(DESTDIR)/$(PREFIX)/$(LIBDIR)
INCLUDE_DEST ?= $(DESTDIR)/$(PREFIX)/$(INCLUDEDIR)
BIN_DEST ?= $(DESTDIR)/$(PREFIX)/$(BINDIR)
SHARE_DEST ?= $(DESTDIR)/$(PREFIX)/$(SHAREDIR)

all: build

build: libasciimap.c
	$(CC) -c -fpic -lorbos-string -o libasciimap.o $(CFLAGS_LIBASCIIMAP) libasciimap.c
	$(CC) -lorbos-string -Wl,-soname=$(SONAME) -shared -o $(SONAME).$(SONAME_VERSION) libasciimap.o
	[ -e "$(SONAME)" ] || ln -s $(SONAME).$(SONAME_VERSION) $(SONAME)
	[ -e "$(SONAME_NOVERSION)" ] || ln -s $(SONAME) $(SONAME_NOVERSION)
	$(CC) -L"." -lorbos-string -lasciimap -oamr $(CFLAGS_AMR) asciimap-render.c

clean: $(SONAME) $(SONAME).$(SONAME_VERSION) $(SONAME_NOVERSION) libasciimap.o amr
	rm $^

rebuild: clean build

install: $(SONAME).$(SONAME_VERSION) $(SONAME_NOVERSION) amr libasciimap.h
	install -Dm755 $(SONAME).$(SONAME_VERSION) $(LIB_DEST)/$(SONAME).$(SONAME_VERSION) 
	install -Dm644 libasciimap.h $(INCLUDE_DEST)/libasciimap.h
	install -Dm755 amr $(BIN_DEST)/amr
	mkdir -p $(SHARE_DEST)/libasciimap
	mv asciimaps $(SHARE_DEST)/libasciimap/asciimaps
	[ -e "$(LIB_DEST)/$(SONAME)" ] || ln -s $(LIB_DEST)/$(SONAME).$(SONAME_VERSION) $(LIB_DEST)/$(SONAME)
	[ -e "$(LIB_DEST)/$(SONAME_NOVERSION)" ] || ln -s $(LIB_DEST)/$(SONAME) $(LIB_DEST)/$(SONAME_NOVERSION)

uninstall: $(LIB_DEST)/$(SONAME) $(LIB_DEST)/$(SONAME).$(SONAME_VERSION) $(INCLUDE_DEST)/libasciimap.h $(BIN_DEST)/amr $(LIB_DEST)/$(SONAME_NOVERSION) $(SHARE_DEST)/libasciimap
	rm -r $^	
