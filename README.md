LibASCIIMap v3.00
==================
LibASCIIMap is a library for reading specific ASCII art and converting it to fancy colored ASCII art. The point is that the ASCII art can still be seen even when not parsing the ASCIIMap.

LibASCIIMap is written in C. Altough it may seem more suitable for this, using a shell script takes about 5 seconds to parse a simple logo without features like comments.

The whole library consists of only one external API function - readAsciiMap(char\* path, char\* pixel) which is used to parse an ASCIIMap file.

Included is a utility for reading ASCII maps built on LibASCIIMap.

Current version of LibASCIIMap and AMR is 3.0.0.

Dependencies
============
* GNU C Compiler (GCC) - `pacman -S gcc` on Arch Linux or `apt-get install gcc` on Debian
* LibOrbos String - C dynamic string library, http://github.com/zatherz/liborbos/
* GNU Make - `pacman -S make` on Arch Linux or `apt-get install make` on Debian

Building
========
To compile, simply execute `make` or `make build`. It will produce five files - libasciimap.o, libasciimap.so.3, libasciimap.so, libasciimap.so.3.0.0 and amr. The file 'libasciimap.o' is the object file that was used to build the shared library, libasciimap.so.3.0.0. Libasciimap.so and libasciimap.so.3 are links to libasciimap.so.3.0.0 that are used to allow ld and gcc to find LibASCIIMap. AMR is the ASCII Map reader.

Other make commands in this Makefile:
* clean - Clean build files
* install - Install built files
* uninstall - Remove built files
* rebuild - Rebuild the project (usually not needed, but may come useful when an update removes some files)

Variables in this Makefile:
* DESTDIR - Destination directory, default: /
* PREFIX - Prefix directory, default: usr
* LIBDIR - Library directory, default: lib
* INCLUDEDIR - C header directory, default: include
* BINDIR - Binary directory, default: bin
* SHAREDIR - Share directory, default: share
* LIB\_DEST - Library destination directory, default: $(DESTDIR)/$(PREFIX)/$(LIBDIR)
* INCLUDE\_DEST - C header destination directory, default: $(DESTDIR)/$(PREFIX)/$(INCLUDEDIR)
* BIN\_DEST - Library destination directory, default: $(DESTDIR)/$(PREFIX)/$(BINDIR)
* SHARE\_DEST - Share destination directory, default: $(DESTDIR)/$(PREFIX)/$(SHAREDIR)

LIB\_DEST, INCLUDE\_DEST and BIN\_DEST override DESTDIR, PREFIX and LIBDIR, INCLUDEDIR or BINDIR.

Linking against LibASCIIMap
===========================
To link against LibASCIIMap, you need to specify the `-L` (library path; if the library is not in the ld search path) and `-l` (library name) options. For example, if `libasciimap.so` exists in the current directory, then to compile and link against LibASCIIMap with your application you need to execute `gcc -L. -lasciimap ./your-source-code.c`.

ASCII Map Format
================
For a quick guide about the format, see asciimaps/format.txt.
