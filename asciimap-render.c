/*
ASCII Map Renderer v1.0
Copyright (C) 2014 Dominik "Zatherz" Banaszak <zatherz at linux dot pl>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#include <stdio.h>
#include <unistd.h>
#include "libasciimap.h"
#include <liborbos-string.h>
#include <getopt.h>
#include <stdlib.h>

const char* version = "1.0.0";

void usage(char* name) {
	printf("usage: %s FILENAME [options]\n", name);
}

void help(char* name) {
	printf("ASCIIMap Renderer %s - render ASCII maps using LibASCIIMap\n", version);
	usage(name);
	printf("       or\n");
	printf("       %s --help|-h|--usage|-u|--version|-v\n", name);
	printf("options:\n");
	printf("--help              | -h        print help and exit\n");
	printf("--pixel str         | -p str    set pixel string\n");
	printf("--background on/off | -b on/off enable/disable background mode\n");
	printf("--usage             | -u        print short usage information and exit\n");
	printf("--version           | -v        print version and exit\n");
}

int main(int argc, char *argv[]) {
	static struct option options[] ={
		{"pixel", required_argument, NULL, 'p'},
		{"background", required_argument, NULL, 'b'},
		{"help", no_argument, NULL, 'h'},
		{"usage", no_argument, NULL, 'u'},
		{"version", no_argument, NULL, 'v'},
		{NULL, 0, NULL, 0}
	};
	int opt;
	int bg = 1;
	string pixelStr = S(" ");
	while ((opt = getopt_long(argc, argv, "huvp:b:", options, NULL)) != -1) {
		switch (opt) {
		case 'p':
			setString(&pixelStr, optarg);
			break;
		case 'b':
			if (strcmp(optarg, "on") == 0) {
				bg = 1;
			} else if (strcmp(optarg, "off") == 0) {
				bg = 0;
			} else {
				fprintf(stderr, "error: wrong argument to option -%c.\n", opt);
				return(1);
			}
			break;
		case 'h':
			help(argv[0]);
			exit(0);
			break;
		case 'u':
			usage(argv[0]);
			exit(0);
			break;
		case 'v':
			printf("ASCIIMapRenderer %s\n", version);
			exit(0);
			break;
		}
	}
	if (argc - optind != 1) {
		usage(argv[0]);
		return 1;
	} else {
		if (bg == 0 && strcmp(getString(&pixelStr), " ") == 0) {
			setString(&pixelStr, "X");
		}
		libasciimapconf conf;
		conf.background = bg;
		conf.pixel = getString(&pixelStr);
		readAsciiMap(argv[optind], conf);
	}
	return 0;
}
