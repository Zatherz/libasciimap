/*
 * ASCII map file format renderer
 * Copyright (C) 2014 Dominik "Zatherz" Banaszak <zatherz at linux dot pl>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include "libasciimap.h"
#include <liborbos-string.h>
#define COLORSHORTLIST_ERR (colorshortlist_t){NULL, 0}


typedef struct syntaxswitches_t {
	int plaintext;
	int comment;
	int newvalue;
	int newcolor;
	int escape;
	int colormod;
	int bgmod;
} syntaxswitches_t;

syntaxswitches_t initSyntaxSwitches() {
	return (syntaxswitches_t){0, 0, 0, 0, 0, 0, 0};
}

typedef struct defaultcolors_t {
        string black;
        string red;
        string green;
        string yellow;
        string brown;
        string orange;
        string blue;
        string magenta;
        string cyan;
        string white;
        string gray;
        string pink;
        string aqua;
        string darkGray;
        string darkGreen;
        string darkRed;
        string darkBlue;
        string darkCyan;
        string darkYellow;
        string darkAqua;
} defaultcolors_t;

defaultcolors_t defaultColors() {
	defaultcolors_t colors;
	colors.black = S("16");
        colors.red = S("196");
        colors.green = S("46");
        colors.yellow = S("226");
        colors.brown = S("202");
        colors.orange = S("208");
        colors.blue = S("21");
        colors.magenta = S("90");
        colors.cyan = S("51");
        colors.white = S("231");
        colors.gray = S("102");
        colors.pink = S("197");
        colors.aqua = S("47");
        colors.darkGray = S("59");
        colors.darkGreen = S("22");
        colors.darkRed = S("52");
        colors.darkBlue = S("17");
        colors.darkCyan = S("23");
        colors.darkYellow = S("220");
        colors.darkAqua = S("42");
	return colors;
}

typedef struct colorshort_t {
	char ch;
	char* color;
} colorshort_t;

typedef struct colorshortlist_t {
	colorshort_t* list;
	int size;
} colorshortlist_t;

colorshortlist_t initColorShortList(defaultcolors_t colorList) {
	colorshortlist_t list;
	colorshort_t* tmplist;
	tmplist = malloc(20 * sizeof(colorshortlist_t));
	if (tmplist == NULL) {
		return COLORSHORTLIST_ERR;
	} else {
		list.list = tmplist;
		list.size = 20;
		list.list[0] = (colorshort_t){'d', getString(&colorList.black)};
	        list.list[1] = (colorshort_t){'r', getString(&colorList.red)};
	        list.list[2] = (colorshort_t){'g', getString(&colorList.green)};
	        list.list[3] = (colorshort_t){'y', getString(&colorList.yellow)};
	        list.list[4] = (colorshort_t){'n', getString(&colorList.brown)};
	        list.list[5] = (colorshort_t){'o', getString(&colorList.orange)};
	        list.list[6] = (colorshort_t){'b', getString(&colorList.blue)};
	        list.list[7] = (colorshort_t){'m', getString(&colorList.magenta)};
	        list.list[8] = (colorshort_t){'c', getString(&colorList.cyan)};
	        list.list[9] = (colorshort_t){'w', getString(&colorList.white)};
	        list.list[10] = (colorshort_t){'a', getString(&colorList.gray)};
	        list.list[11] = (colorshort_t){'p', getString(&colorList.pink)};
	        list.list[12] = (colorshort_t){'q', getString(&colorList.aqua)};
	        list.list[13] = (colorshort_t){'A', getString(&colorList.darkGray)};
	        list.list[14] = (colorshort_t){'G', getString(&colorList.darkGreen)};
	        list.list[15] = (colorshort_t){'R', getString(&colorList.darkRed)};
	        list.list[16] = (colorshort_t){'B', getString(&colorList.darkBlue)};
	        list.list[17] = (colorshort_t){'C', getString(&colorList.darkCyan)};
	        list.list[18] = (colorshort_t){'Y', getString(&colorList.darkYellow)};
	        list.list[19] = (colorshort_t){'Q', getString(&colorList.darkAqua)};
		return list;
	}
}

int setColorShort(colorshortlist_t* list, char ch, char* color) {
	int i;
	for (i = 0; i < list->size; i++) {
		if (list->list[i].ch == ch) {
			list->list[i].color = color;
			return 0;
		}
	}
	colorshort_t* tmpShort;
	int newSize = list->size + 1;
	tmpShort = realloc(list->list, newSize * sizeof(colorshort_t));
	if (tmpShort == NULL) {
		return 0;
	} else {
		list->size = newSize;
		list->list = tmpShort;
		list->list[newSize-1].ch = ch;
		char* mallocTmp;
		mallocTmp = malloc((strlen(color) + 1) * sizeof(char*));
		if (mallocTmp == NULL) {
			return 0;
		}
		list->list[newSize-1].color = mallocTmp;
		strcpy(list->list[newSize-1].color, color);
	}
	return 1;
}

char* color(int background, char* color) {
	string tmp = S("\e[");
	if (strcmp(color, "reset") == 0) {
		return "\e[0m";
	}
	if (!background) {
		catStringChar(&tmp, "38;5;");
	} else {
		catStringChar(&tmp, "48;5;");
	}
	catStringChar(&tmp, color);
	catStringChar(&tmp, "m");
	return(getString(&tmp));
}

char* getColor(colorshortlist_t* list, char ch) {
	int i;
	for (i = 0; i < list->size; i++) {
		if (list->list[i].ch == ch) {
			return list->list[i].color;
		}
	}
	return "0";
}

char* colorShort(colorshortlist_t* list, int background, char ch) {
	return color(background, getColor(list, ch));
}

int isValidColorShortList(colorshortlist_t* list) {
	if (list->size == 0 && list->list == NULL) {
		return 1;
	} else {
		return 0;
	}
}


int readAsciiMap(char* path, libasciimapconf conf) {
	FILE* asciiMap;
	int lastc;
	int c;
	defaultcolors_t colors = defaultColors();
	colorshortlist_t shorts = initColorShortList(colors);
	syntaxswitches_t sw = initSyntaxSwitches();
	char* pixel = conf.pixel;
	int background = conf.background;
	char newColorShort;
	string newColorValue = S("");
	if (strcmp(pixel, "") == 0 && background != 1) {
		background = 1;
		pixel = " ";
	}
	asciiMap = fopen(path, "r");
	if (!asciiMap) {
		return 1;
	} else {
		char* term = getenv("TERM");
		if (strcmp(term, "xterm256") != 0 && strcmp(term, "xterm") != 0 && strcmp(term, "rxvt-unicode-256color") != 0) {
			fprintf(stderr, "warning: you're running a possibly not xterm256/urxvt256 compatible terminal, which may break LibASCIIMap; if the colors won't show up correctly, please try another terminal\n");
		}
		while ((c = fgetc(asciiMap)) != EOF) {
			if (sw.escape) {
				if (c == '\n') {
					sw.escape = 0;
					continue;
				}
				if (!sw.comment) {
					printf("%c", c);
				}
				sw.escape = 0;
				continue;
			}
			
			switch(c) {
				case '[':
					if (! sw.comment && ! sw.plaintext) {
						sw.plaintext = 1;
						continue;
					}
					break;
				case ']':
					if (! sw.comment) {
						sw.plaintext = 0;
						continue;
					}
					break;
				case '\n':
					printf("\n");
					continue;
				case '#':
					if (! sw.plaintext) {
						sw.comment = sw.comment ? 0 : 1;
						continue;
					}
					break;
				case '\\':
					sw.escape = sw.escape ? 0 : 1;
					continue;
				case '|':
					if (! sw.plaintext && ! sw.comment ) {
						sw.colormod = 1;
						continue;
					}
					break;
				case '&':
					if (! sw.plaintext && ! sw.comment ) {
						sw.bgmod = 1;
						continue;
					}
					break;
				case '!':
					if (! sw.plaintext && ! sw.comment) {
						printf("\e[0m");
						continue;
					}
					break;
				case ':':
					if (! sw.plaintext && ! sw.comment) {
						sw.newcolor = 1;
						continue;
					}
					break;
				case '=':
					if (! sw.plaintext && ! sw.comment) {
						if (sw.newcolor) {
							sw.newcolor = 0;
							sw.newvalue = 1;
						}
						continue;
					}
					break;
				case ';':
					if (! sw.plaintext && ! sw.comment) {
						if (sw.newvalue) {
							setColorShort(&shorts, newColorShort, getString(&newColorValue));
							sw.newvalue = 0;
							setString(&newColorValue, "");
							newColorShort = '\0';
						}
						continue;
					}
					break;
				case ' ': // or
				case '\t':
					if (!sw.comment) {
						printf("%c", c);
					}
					continue;
			}
			if (sw.bgmod) {
				printf("%s", colorShort(&shorts, 1, c));
				sw.bgmod = 0;
				continue;
			}
			if (sw.colormod) {
				printf("%s", colorShort(&shorts, 0, c));
				sw.colormod = 0;
				continue;
			}
			if (sw.newcolor) {
				newColorShort = c;
				continue;
			}
			if (sw.newvalue) {
				if (isalpha(c)) {
					setString(&newColorValue, "");
					setColorShort(&shorts, newColorShort, getColor(&shorts, c));
					sw.newvalue = 0;
					newColorShort = '\0';
					continue;
				}
				appendStringChar(&newColorValue, c);
				continue;
			}

			if (sw.plaintext) {
				printf("%c", c);
				continue;
			}
			if (sw.comment) {
				continue;
			}
			printf("%s%s%s%s", color(background, "reset"), colorShort(&shorts, background, c), pixel, color(background, "reset"));
		}
	}
	return 0;
}
